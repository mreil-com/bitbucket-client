package com.mreil.bitbucket.api;

import com.mreil.bitbucket.credentials.Hook;
import com.mreil.bitbucket.credentials.Paginated;
import com.mreil.bitbucket.credentials.PipelineVariable;
import com.mreil.bitbucket.credentials.SshKey;
import com.mreil.bitbucket.credentials.User;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

@Headers("Content-Type: application/json")
public interface UsersEndpoint {
    @RequestLine("GET /users/{username}")
    User user(@Param("username") String username);

    @RequestLine("GET /users/{username}/followers")
    Paginated<User> followers(@Param("username") String username);

    @RequestLine("GET /users/{username}/following")
    Paginated<User> following(@Param("username") String username);

    @RequestLine("GET /users/{username}/hooks")
    Paginated<Hook> hooks(@Param("username") String username);

    @RequestLine("POST /users/{username}/hooks")
    Hook createHook(@Param("username") String username,
                    CreateHookCommand createHookCommand);

    @RequestLine("GET /users/{username}/hooks/{uid}")
    Hook hook(@Param("username") String username,
              @Param("uid") String uid);

    @RequestLine("DELETE /users/{username}/hooks/{uid}")
    Void deleteHook(@Param("username") String username,
              @Param("uid") String uid);

    @RequestLine("PUT /users/{username}/hooks/{uid}")
    Hook modifyHook(@Param("username") String username,
                    @Param("uid") String uid,
                    ModifyHookCommand modifyHookCommand);

    @RequestLine("GET /users/{username}/members")
    Paginated<User> members(@Param("username") String username);

    @RequestLine("GET /users/{username}/pipelines_config/variables/")
    Paginated<PipelineVariable> pipelinesVariables(@Param("username") String username);
    // TODO POST

    @RequestLine("GET /users/{username}/pipelines_config/variables/{variableUuid}")
    PipelineVariable pipelinesVariable(@Param("username") String username,
                                       @Param("variableUuid") String variableUuid);
    // TODO PUT, DELETE

    @RequestLine("GET /users/{username}/ssh-keys")
    Paginated<SshKey> sshKeys(@Param("username") String username);
    // TODO POST

}
