package com.mreil.bitbucket.api;

import com.mreil.bitbucket.credentials.Email;
import com.mreil.bitbucket.credentials.Paginated;
import com.mreil.bitbucket.credentials.RepositoryPermission;
import com.mreil.bitbucket.credentials.TeamPermission;
import com.mreil.bitbucket.credentials.User;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

@Headers("Content-Type: application/json")
public interface UserEndpoint {
    @RequestLine("GET /user")
    User user();

    @RequestLine("GET /user/emails")
    Paginated<Email> emails();

    @RequestLine("GET /user/emails/{emailAddress}")
    Email emails(@Param("emailAddress") String email);

    @RequestLine("GET /user/permissions/repositories")
    Paginated<RepositoryPermission> repositoryPermissions();

    @RequestLine("GET /user/permissions/teams")
    Paginated<TeamPermission> teamPermissions();
}
