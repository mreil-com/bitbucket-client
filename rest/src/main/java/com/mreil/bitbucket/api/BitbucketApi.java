package com.mreil.bitbucket.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mreil.bitbucket.credentials.Anonymous;
import com.mreil.bitbucket.credentials.CredentialsProvider;
import com.mreil.bitbucket.credentials.UserPass;
import feign.Feign;
import feign.Logger;
import feign.RequestInterceptor;
import feign.auth.BasicAuthRequestInterceptor;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Root class to access different API endpoints.
 */
@Slf4j
@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class BitbucketApi {

    public static final String BASE_URL = "https://api.bitbucket.org/2.0";

    @Builder.Default
    @NonNull
    private String baseUrl = BASE_URL;

    @Builder.Default
    @NonNull
    private CredentialsProvider credentialsProvider = new Anonymous();

    @Singular
    private List<RequestInterceptor> interceptors;

    private Feign.Builder defaultConfig() {
        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .registerModule(new Jdk8Module())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Feign.Builder builder = Feign.builder()
                .encoder(new JacksonEncoder(mapper))
                .decoder(new JacksonDecoder(mapper))
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL);

        builder.requestInterceptors(interceptors);
        // TODO make nicer
        if (credentialsProvider instanceof UserPass) {
            builder.requestInterceptor(new BasicAuthRequestInterceptor(
                    (((UserPass) credentialsProvider).getUsername()),
                    (((UserPass) credentialsProvider).getPassword())
            ));
        }

        return builder;
    }

    /**
     * Get the /user endpoint.
     */
    public UserEndpoint getUserEndpoint() {
        return defaultConfig()
                .target(UserEndpoint.class, baseUrl);
    }

    /**
     * Get the /users endpoint.
     */
    public UsersEndpoint getUsersEndpoint() {
        return defaultConfig()
                .target(UsersEndpoint.class, baseUrl);
    }
}
