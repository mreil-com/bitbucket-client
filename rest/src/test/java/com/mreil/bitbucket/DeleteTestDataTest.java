package com.mreil.bitbucket;

import com.mreil.bitbucket.api.BitbucketApi;
import com.mreil.bitbucket.credentials.Hook;
import com.mreil.bitbucket.credentials.Paginated;
import com.mreil.bitbucket.credentials.UserPass;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

// TODO do not run as test
class DeleteTestDataTest {
    private final BitbucketApi api = BitbucketApi.builder()
            .credentialsProvider(UserPass.builder()
                    .username(System.getProperty("bitbucket.username"))
                    .password(System.getProperty("bitbucket.password"))
                    .build())
            .build();

    @Test
    @Disabled
    void deleteTestHooks() {
        Paginated<Hook> hooks;
        do {
            hooks = api.getUsersEndpoint().hooks("mreilbbclient");
            hooks.getValues()
                    .stream()
                    .filter(hook -> hook.getDescription().equals("Test Hook"))
                    .forEach(hook -> api.getUsersEndpoint()
                            .deleteHook("mreilbbclient", hook.getUuid()));
        } while (hooks.getSize() == hooks.getPagelen());
    }
}
