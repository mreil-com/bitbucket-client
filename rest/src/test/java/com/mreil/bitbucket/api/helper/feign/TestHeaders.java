package com.mreil.bitbucket.api.helper.feign;

import com.github.tomakehurst.wiremock.matching.MultiValuePattern;
import com.github.tomakehurst.wiremock.recording.RecordSpecBuilder;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.mreil.bitbucket.api.helper.jupiter.TestRequestContext;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import static com.mreil.bitbucket.api.helper.feign.TestHeaders.HeaderName.TEST_CLASS;
import static com.mreil.bitbucket.api.helper.feign.TestHeaders.HeaderName.TEST_METHOD_NAME;
import static com.mreil.bitbucket.api.helper.feign.TestHeaders.HeaderName.TEST_REQUEST_COUNT;
import static com.mreil.bitbucket.api.helper.feign.TestHeaders.HeaderName.TEST_REQUEST_ID;

@Slf4j
public class TestHeaders {

    enum HeaderName {
        TEST_REQUEST_ID("testRequestId"),
        TEST_CLASS("testClass"),
        TEST_METHOD_NAME("testMethodName"),
        TEST_REQUEST_COUNT("testRequestCount");

        private final String headerName;

        HeaderName(String headerName) {
            this.headerName = headerName;
        }
    }

    /**
     * Configures WireMock so that all headers from the enum are captured.
     */
    public static RecordSpecBuilder captureHeaders(RecordSpecBuilder spec) {
        Arrays.stream(HeaderName.values())
                .forEach(h -> spec.captureHeader(h.headerName));
        return spec;
    }

    static void insertTestHeaders(RequestTemplate template,
                                  TestRequestContext requestContext) {
        template.header(TEST_CLASS.headerName, requestContext.getTmc().getTcc().getTestClass());
        template.header(TEST_METHOD_NAME.headerName, requestContext.getTmc().getTestMethodName());
        template.header(TEST_REQUEST_ID.headerName, requestContext.getRequestId());
        template.header(TEST_REQUEST_COUNT.headerName, String.valueOf(requestContext.getCount()));
    }

    /**
     * Extract test headers from stub mapping.
     * @param mapping the stub mapping of the current HTTP request
     * @param rootPath root dir of the stub mapping files (usually src/test/resources)
     * @return test request context
     */
    public static TestRequestContext extractHeaders(StubMapping mapping, String rootPath) {
        Map<String, MultiValuePattern> headers = mapping.getRequest().getHeaders();
        return TestRequestContext.of(
                Paths.get(rootPath),
                extract(headers, TEST_CLASS),
                extract(headers, TEST_METHOD_NAME),
                extract(headers, TEST_REQUEST_ID),
                Integer.parseInt(extract(headers, TEST_REQUEST_COUNT)));
    }

    private static String extract(Map<String, MultiValuePattern> headers, HeaderName key) {
        return Optional.ofNullable(headers.get(key.headerName).getExpected())
                .orElseThrow(() -> new RuntimeException("Header not present: " + key));
    }
}
