package com.mreil.bitbucket.api.testdata;

import com.mreil.bitbucket.credentials.BaseUser;
import com.mreil.bitbucket.credentials.User;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Contains expected data.
 * <p>
 * Moving to another test account should be easier with everything in one spot.
 */
public class TestData {
    public static final String USERACCOUNT_USERNAME = "mreilbbclient";
    public static final String USERACCOUNT_DISPLAYNAME = "Markus Reil (bbclient)";
    public static final String USERACCOUNT_ACCOUNT_ID = "557058:bc4e8b4d-2fcb-4ac2-98a3-1d7036d25e9b";
    public static final String USERACCOUNT_UUID = "{34e1d254-79ba-4353-80cf-cf562685320d}";
    public static final String USERACCOUNT_EMAIL = "markus.reil+bbclient@gmail.com";

    public static final String TEAMACCOUNT_USERNAME = "mreilbbclientteam";
    public static final String TEAMACCOUNT_DISPLAYNAME = "mreilbbclientteam";
    public static final String TEAMACCOUNT_UUID = "{bdbead1f-621f-494c-908e-f055999b2844}";

    public static final String REPOSITORY_NAME = "REPO";
    public static final String REPOSITORY_FULLNAME = "mreilbbclient/repo";
    public static final String REPOSITORY_UUID = "{1d28c810-be62-4841-ab1d-a692c9c1e839}";

    public static final BaseUser EXPECTED_BASE_USER = BaseUser.builder()
            .username(USERACCOUNT_USERNAME)
            .display_name(USERACCOUNT_DISPLAYNAME)
            .account_id(USERACCOUNT_ACCOUNT_ID)
            .type("user")
            .uuid(USERACCOUNT_UUID)
            .build();

    public static final User EXPECTED_USER = User.builder()
            .username(USERACCOUNT_USERNAME)
            .website(Optional.empty())
            .displayName(USERACCOUNT_DISPLAYNAME)
            .accountId(USERACCOUNT_ACCOUNT_ID)
            .isStaff(false)
            .type("user")
            .uuid(USERACCOUNT_UUID)
            .location(Optional.empty())
            .createdOn(ZonedDateTime.parse("2017-09-25T15:29:44.011430+00:00")
                    .withZoneSameInstant(ZoneId.of("UTC")))
            .build();
}
