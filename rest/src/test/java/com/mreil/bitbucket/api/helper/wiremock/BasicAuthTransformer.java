package com.mreil.bitbucket.api.helper.wiremock;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.StubMappingTransformer;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.github.tomakehurst.wiremock.matching.EqualToPattern;
import com.github.tomakehurst.wiremock.matching.MultiValuePattern;
import com.github.tomakehurst.wiremock.matching.RequestPattern;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import org.apache.http.HttpHeaders;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Change Authorization header.
 * If header is present and result is 2xx then translate real Basic Auth header to dummy header
 * (s. {@link CredentialsHelper#correctUserPassForLocalTests()}.
 */
public class BasicAuthTransformer extends StubMappingTransformer {
    @Override
    public StubMapping transform(StubMapping stubMapping, FileSource files, Parameters parameters) {
        // TODO
        RequestPattern req = stubMapping.getRequest();
        RequestPattern requestPattern = new RequestPattern(
                req.getUrlMatcher(),
                req.getMethod(),
                replaceHeaders(stubMapping),
                req.getQueryParameters(),
                req.getCookies(),
                req.getBasicAuthCredentials(),
                req.getBodyPatterns(),
                req.getCustomMatcher(),
                req.getMultipartPatterns()
        );

        ResponseDefinition rd = stubMapping.getResponse();
        ResponseDefinition responseDefinition = new ResponseDefinition(
                rd.getStatus(),
                rd.getStatusMessage(),
                rd.getBody(),
                null, // TODO?
                rd.getBase64Body(),
                rd.getBodyFileName(),
                rd.getHeaders(),
                rd.getAdditionalProxyRequestHeaders(),
                rd.getFixedDelayMilliseconds(),
                rd.getDelayDistribution(),
                rd.getChunkedDribbleDelay(),
                rd.getProxyBaseUrl(),
                rd.getFault(),
                rd.getTransformers(),
                rd.getTransformerParameters(),
                rd.wasConfigured()
        );

        return new StubMapping(requestPattern,
                responseDefinition);
    }

    private Map<String, MultiValuePattern> replaceHeaders(StubMapping stubMapping) {
        if (containsRealCredentials(stubMapping)) {
            return replaceHeaders(stubMapping.getRequest().getHeaders());
        } else {
            return stubMapping.getRequest().getHeaders();
        }
    }

    private Map<String, MultiValuePattern> replaceHeaders(Map<String, MultiValuePattern> headers) {
        if (headers == null || headers.isEmpty()) {
            return headers;
        }
        return headers.entrySet().stream()
                .map(e -> e.getKey().equals(HttpHeaders.AUTHORIZATION)
                        ? new AbstractMap.SimpleEntry<>(e.getKey(),
                                new MultiValuePattern(new EqualToPattern("Basic dXNlcm5hbWU6Y29ycmVjdHBhc3M=")))
                        : e)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private boolean containsRealCredentials(StubMapping stubMapping) {
        // TODO
        return true;
    }

    @Override
    public String getName() {
        return "BasicAuthTransformer";
    }
}
