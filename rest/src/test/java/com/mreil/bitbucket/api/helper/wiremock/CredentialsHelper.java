package com.mreil.bitbucket.api.helper.wiremock;

import com.mreil.bitbucket.credentials.CredentialsProvider;
import com.mreil.bitbucket.credentials.UserPass;

class CredentialsHelper {

    /**
     * Dummy credentials that are assumed to be correct for local tests.
     *
     * @return a CredentialsProvider with username and password.
     */
    static CredentialsProvider correctUserPassForLocalTests() {
        return UserPass.builder()
                .username("username")
                .password("correctpass")
                .build();
    }

    /**
     * Credentials for the live API, provided through system properties.
     *
     * @return a CredentialsProvider with username and password.
     */
    static CredentialsProvider correctUserPassForApi() {
        return UserPass.builder()
                .username(System.getProperty("bitbucket.username"))
                .password(System.getProperty("bitbucket.password"))
                .build();
    }
}
