package com.mreil.bitbucket.api.helper.wiremock;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.stream.Collectors;

import static org.apache.http.HttpHeaders.AUTHORIZATION;

@Slf4j
public class StupidTransformer extends ResponseTemplateTransformer {

    public StupidTransformer() {
        super(true);
    }

    @Override
    public ResponseDefinition transform(Request request,
                                        ResponseDefinition responseDefinition,
                                        FileSource files,
                                        Parameters parameters) {
        /*
        byte[] byteBody = responseDefinition.getByteBody();

        log.info(String.valueOf(byteBody));

        ResponseDefinition rd = new ResponseDefinition(
                responseDefinition.getStatus(),
                responseDefinition.getStatusMessage(),
                "anc",
                JsonNodeFactory.instance.textNode("blablabla"),
                "",
                responseDefinition.getBodyFileName(),
                responseDefinition.getHeaders(),
                responseDefinition.getAdditionalProxyRequestHeaders(),
                responseDefinition.getFixedDelayMilliseconds(),
                responseDefinition.getDelayDistribution(),
                responseDefinition.getChunkedDribbleDelay(),
                responseDefinition.getProxyBaseUrl(),
                responseDefinition.getFault(),
                responseDefinition.getTransformers(),
                responseDefinition.getTransformerParameters(),
                responseDefinition.wasConfigured()
        );

        Request r = new LoggedRequest(
                //request.getUrl(),
                "mehmeh",
                request.getAbsoluteUrl(),
                RequestMethod.DELETE, // request.getMethod(),
                request.getClientIp(),
                transformHeaders(request),
                request.getCookies(),
                true, // TODO ?
                null,
                request.getBody(),
                request.getParts()
        );

        rd.setOriginalRequest(r);

        return rd;
        */
        return responseDefinition;
    }

    private HttpHeaders transformHeaders(Request request) {
        return new HttpHeaders(request.getHeaders().all()
                .stream()
                .map(h -> changeAuth(h))
                .collect(Collectors.toList()));
    }

    private HttpHeader changeAuth(HttpHeader header) {
        return Optional.of(header)
                .filter(h -> h.keyEquals(AUTHORIZATION))
                .map(h -> new HttpHeader(header.key(), "somevalue"))
                .orElse(header);
    }

    @Override
    public String getName() {
        return "blerg";
    }
}
