package com.mreil.bitbucket.api.helper.wiremock;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.admin.model.GetServeEventsResult;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.common.SingleRootFileSource;
import com.github.tomakehurst.wiremock.common.Slf4jNotifier;
import com.github.tomakehurst.wiremock.recording.RecordSpec;
import com.github.tomakehurst.wiremock.recording.RecordSpecBuilder;
import com.github.tomakehurst.wiremock.recording.RecordingStatus;
import com.mreil.bitbucket.api.BitbucketApi;
import com.mreil.bitbucket.api.helper.Env;
import com.mreil.bitbucket.api.helper.feign.TestHeaders;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.any;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

@Slf4j
class WireMockSetupHelper {
    private final Path rootDir;
    private WireMockServer mockServer;

    private WireMockSetupHelper(Path rootDir) {
        this.rootDir = rootDir;
    }

    /**
     * Configure WireMock for testing.
     *
     * @param rootDir directory where stub mappings are persisted.
     */
    static WireMockSetupHelper atRoot(Path rootDir) {
        WireMockSetupHelper helper = new WireMockSetupHelper(rootDir);
        helper.setup();
        return helper;
    }

    private void setup() {
        if (mockServer != null) {
            throw new RuntimeException("already initialised");
        }

        FileSource root = new SingleRootFileSource(rootDir.toString());

        mockServer = new WireMockServer(
                options()
                        .dynamicPort()
                        .mappingSource(new TestAwareFileMappingsSource(root))
                        .extensions(new StupidTransformer(),
                                new BasicAuthTransformer())
                        // TODO use test class here and remove from TestAwareFileMappingsSource
                        .fileSource(root)
                        .notifier(new Slf4jNotifier(true))
        );
        mockServer.start();

        if (Env.isLive()) {
            // TODO more info
            log.info("The property 'liveTest' is set.");
            configureRewritingProxy(mockServer);
        } else {
            configureLocalDocumentStore();
        }
    }

    /**
     * Configure WireMock as proxy to the actual API.
     * // TODO what else is being configured
     *
     * @param server the WireMock instance
     */
    private static void configureRewritingProxy(WireMockServer server) {
        server.stubFor(any(urlMatching(".*")).atPriority(10)
                .willReturn(aResponse().proxiedFrom(BitbucketApi.BASE_URL)));

        RecordSpec spec = TestHeaders.captureHeaders(new RecordSpecBuilder()
                .forTarget(BitbucketApi.BASE_URL)
                .makeStubsPersistent(true)
                .captureHeader("Authorization"))
                .build();
        server.startRecording(spec);
    }

    private static void configureLocalDocumentStore() {
    }

    /**
     * Stop recording and shut down WireMock server.
     */
    void shutdown() {
        if (mockServer.getRecordingStatus().getStatus().equals(RecordingStatus.Recording)) {
            mockServer.stopRecording();
        }
        mockServer.shutdown();

    }

    GetServeEventsResult getServeEvents() {
        return mockServer.getServeEvents();
    }

    int port() {
        return mockServer.port();
    }
}
