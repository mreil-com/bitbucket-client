package com.mreil.bitbucket.api;

import com.mreil.bitbucket.api.helper.wiremock.WireMockRule;
import com.mreil.bitbucket.api.testdata.TestData;
import com.mreil.bitbucket.credentials.Hook;
import com.mreil.bitbucket.credentials.Paginated;
import com.mreil.bitbucket.credentials.PipelineVariable;
import com.mreil.bitbucket.credentials.SshKey;
import com.mreil.bitbucket.credentials.User;
import feign.FeignException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.Collections;
import java.util.function.Supplier;

import static com.mreil.bitbucket.api.Event.ISSUE_CREATED;
import static com.mreil.bitbucket.api.Event.REPO_DELETED;
import static org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UsersEndpointTest {
    @SuppressWarnings("WeakerAccess")
    @RegisterExtension
    static final WireMockRule mock = new WireMockRule("src/test/resources");

    private static UsersEndpoint loggedIn;
    private static UsersEndpoint anonymous;

    @BeforeAll
    static void getApiRoots() {
        loggedIn = mock.getApiRoot().getUsersEndpoint();
        anonymous = mock.getApiRootAnonymous().getUsersEndpoint();
    }

    @Test
    @DisplayName("Get user by username")
    void user() {
        User user = makeCall(() -> anonymous.user("mreilbbclient"),
                "call to user");

        assertThat(user).isEqualToIgnoringNullFields(TestData.EXPECTED_USER);
    }

    @Test
    @DisplayName("Get followers by username")
    void followers() {
        Paginated<User> response = makeCall(() -> anonymous.followers("mcannonbrookes"),
                "call to followers");

        assertThat(response.getValues()).isNotEmpty();
    }

    @Test
    @DisplayName("Get following by username")
    void following() {
        Paginated<User> response = makeCall(() -> anonymous.following("mcannonbrookes"),
                "call to following");

        assertThat(response.getValues()).isNotEmpty();
    }

    @Test
    @DisplayName("Get hooks by username")
    void hooks() {
        Paginated<Hook> response = makeCall(() -> loggedIn.hooks("mreilbbclient"),
                "call to hooks");

        assertThat(response).isNotNull();
    }

    @Test
    @DisplayName("Create hook")
    void createHook() {
        // create
        Hook response = makeCall(() -> loggedIn.createHook("mreilbbclient",
                CreateHookCommand.builder()
                        .description("Test Hook")
                        .events(Collections.singleton(ISSUE_CREATED))
                        .url("http://www.bitbucket.org")
                        .active(true)
                        .build()),
                "post to hooks");

        // verify found
        Hook hook = makeCall(() -> loggedIn.hook("mreilbbclient", response.getUuid()),
                        "retrieve hook");
        assertThat(hook).isNotNull();

        // cleanup
        makeCall(() -> loggedIn.deleteHook("mreilbbclient", hook.getUuid()),
                "delete created hook");
    }

    @Test
    @DisplayName("Delete hook")
    void deleteHook() {
        // create
        Hook response = makeCall(() -> loggedIn.createHook("mreilbbclient",
                CreateHookCommand.builder()
                        .description("Test Hook")
                        .events(Collections.singleton(ISSUE_CREATED))
                        .url("http://www.bitbucket.org")
                        .build()),
                "post to hooks");

        // delete
        makeCall(() -> loggedIn.deleteHook("mreilbbclient",
                response.getUuid()),
                "delete hook");

        // verify not found
        FeignException ex = assertThrows(FeignException.class, () ->
                makeCall(() -> loggedIn.hook("mreilbbclient", response.getUuid()),
                        "retrieve hook"));
        assertThat(ex.status()).isEqualTo(SC_NOT_FOUND);
    }

    @Test
    @DisplayName("Modify hook")
    void modifyHook() {
        // create
        Hook created = makeCall(() -> loggedIn.createHook("mreilbbclient",
                CreateHookCommand.builder()
                        .description("Test Hook")
                        .events(Collections.singleton(ISSUE_CREATED))
                        .url("http://www.bitbucket.org")
                        .active(false)
                        .build()),
                "post to hooks");

        // modify
        Hook hook = makeCall(() -> loggedIn.modifyHook("mreilbbclient",
                created.getUuid(),
                ModifyHookCommand.builder()
                        .events(Collections.singleton(REPO_DELETED))
                        .active(created.isActive())
                        .url(created.getUrl())
                        .description(created.getDescription())
                        .build()),
                "put to hooks");

        // verify modified
        Hook modified = makeCall(() -> loggedIn.hook("mreilbbclient", hook.getUuid()),
                "retrieve hook");
        assertThat(modified.getEvents()).containsOnly(REPO_DELETED);
    }

    @Test
    @DisplayName("Get hooks by username anonymously")
    void hooks_anonymous() {
        FeignException ex = assertThrows(FeignException.class,
                () -> makeCall(() -> anonymous.hooks("mreilbbclient"),
                        "call to hooks anonymously"));

        assertThat(ex.status()).isEqualTo(SC_UNAUTHORIZED);
    }

    @Test
    @DisplayName("Get hook by username and uid")
    void hook() {
        Hook response = makeCall(() -> loggedIn.hook("mreilbbclient",
                "{a2b3c3c0-aa06-48fe-bc00-9992ce0b86a1}"),
                "call to hooks/uid");

        assertThat(response).isNotNull();
    }

    @Test
    @DisplayName("Get hook by username and uid anonymously fails")
    void hook_anonymous() {
        FeignException ex = assertThrows(FeignException.class,
                () -> makeCall(() -> anonymous.hook("mreilbbclient",
                        "{a2b3c3c0-aa06-48fe-bc00-9992ce0b86a1}"),
                        "call to hooks/uid anonymously"));

        assertThat(ex.status()).isEqualTo(SC_UNAUTHORIZED);
    }

    @Test
    @DisplayName("Get members by teamname")
    void members() {
        Paginated<User> response = makeCall(() -> anonymous.members("mreilbbclientteam"),
                "call to members");

        assertThat(response.getValues().stream()
                .filter(m -> m.getUsername().equals(TestData.USERACCOUNT_USERNAME))
                .findFirst().orElseThrow(() -> new RuntimeException("No user")))
                .isEqualToIgnoringNullFields(TestData.EXPECTED_USER);
    }

    @Test
    @DisplayName("Get pipelines variables by username")
    void pipelinesVariables() {
        Paginated<PipelineVariable> response = makeCall(
                () -> loggedIn.pipelinesVariables("mreilbbclient"),
                "call to pipelines variables");

        assertThat(response).isNotNull();
    }

    @Test
    @DisplayName("Get pipelines variables by username anonymously fails")
    void pipelinesVariables_anonymous() {
        FeignException ex = assertThrows(FeignException.class,
                () -> makeCall(() -> anonymous.pipelinesVariables("mreilbbclient"),
                        "call to pipelines variables anonymously"));

        assertThat(ex.status()).isEqualTo(SC_NOT_FOUND);
    }

    @Test
    @DisplayName("Get pipelines variable by username and uid")
    void pipelinesVariable() {
        PipelineVariable response = makeCall(() -> loggedIn.pipelinesVariable("mreilbbclient",
                "{2960672d-48c9-4425-a10b-6a4834d419a1}"),
                "call to pipelines variable");

        assertThat(response).isNotNull();
    }

    @Test
    @DisplayName("Get pipelines variable by username and uid anonymously fails")
    void pipelinesVariable_anonymous() {
        FeignException ex = assertThrows(FeignException.class,
                () -> makeCall(() -> anonymous.pipelinesVariable("mreilbbclient",
                        "{2960672d-48c9-4425-a10b-6a4834d419a1}"),
                        "call to pipelines variable"));

        assertThat(ex.status()).isEqualTo(SC_NOT_FOUND);
    }

    @Test
    @DisplayName("Get SSH keys by username")
    void sshKeys() {
        Paginated<SshKey> response = makeCall(() -> loggedIn.sshKeys("mreilbbclient"),
                "call to ssh-keys");

        assertThat(response).isNotNull();
    }

    @Test
    @DisplayName("Get SSH keys by username anonymously fails")
    void sshKeys_anonymous() {
        FeignException ex = assertThrows(FeignException.class,
                () -> makeCall(() -> anonymous.sshKeys("mreilbbclient"),
                        "call to ssh-keys anonymously"));

        assertThat(ex.status()).isEqualTo(SC_INTERNAL_SERVER_ERROR);
    }


    private <X> X makeCall(Supplier<X> call, String id) {
        mock.nextRequest(id);
        return call.get();
    }
}
