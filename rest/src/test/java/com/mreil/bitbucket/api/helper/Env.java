package com.mreil.bitbucket.api.helper;

public class Env {
    public static boolean isLive() {
        return Boolean.TRUE.toString().equals(System.getProperty("liveTest"));
    }
}
