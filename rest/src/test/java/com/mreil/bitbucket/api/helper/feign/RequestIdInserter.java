package com.mreil.bitbucket.api.helper.feign;

import com.mreil.bitbucket.api.helper.jupiter.TestRequestContext;
import feign.RequestInterceptor;
import feign.RequestTemplate;

public class RequestIdInserter implements RequestInterceptor {
    private final ThreadLocal<TestRequestContext> context;

    public RequestIdInserter(ThreadLocal<TestRequestContext> context) {
        this.context = context;
    }

    @Override
    public void apply(RequestTemplate template) {
        TestHeaders.insertTestHeaders(template,
                context.get());
    }
}
