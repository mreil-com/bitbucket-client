package com.mreil.bitbucket.api.helper.jupiter;

import lombok.Data;
import lombok.SneakyThrows;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Data
public class TestClassContext {
    private Path rootDir;
    private String testClass;

    @SneakyThrows(IOException.class)
    private TestClassContext(Path rootDir, String testClass) {
        this.rootDir = rootDir;
        this.testClass = testClass;
        Files.createDirectories(rootDir);
    }

    static TestClassContext of(Path rootDir, String testClass) {
        return new TestClassContext(rootDir, testClass);
    }

    public static TestClassContext fromContext(
            Path rootDir,
            ExtensionContext ec) {
        return new TestClassContext(rootDir,
                ec.getRequiredTestClass().getCanonicalName());
    }

    Path getRelativePath() {
        return Paths
                .get(testClass.replace(".", File.separator));
    }

    public Path getAbsolutePath() {
        return rootDir.resolve(getRelativePath());
    }

}
