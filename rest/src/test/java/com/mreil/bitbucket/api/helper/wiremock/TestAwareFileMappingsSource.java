package com.mreil.bitbucket.api.helper.wiremock;

import com.github.tomakehurst.wiremock.common.AbstractFileSource;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.common.JsonException;
import com.github.tomakehurst.wiremock.common.TextFile;
import com.github.tomakehurst.wiremock.standalone.MappingFileException;
import com.github.tomakehurst.wiremock.standalone.MappingsSource;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.github.tomakehurst.wiremock.stubbing.StubMappings;
import com.mreil.bitbucket.api.helper.feign.TestHeaders;
import com.mreil.bitbucket.api.helper.jupiter.TestRequestContext;
import lombok.SneakyThrows;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.github.tomakehurst.wiremock.common.Json.writePrivate;

public class TestAwareFileMappingsSource implements MappingsSource {

    private final FileSource mappingsFileSource;
    private final Map<UUID, String> fileNameMap;

    TestAwareFileMappingsSource(FileSource mappingsFileSource) {
        this.mappingsFileSource = mappingsFileSource;
        fileNameMap = new HashMap<>();
    }

    @Override
    public void save(List<StubMapping> stubMappings) {
        for (StubMapping mapping : stubMappings) {
            if (mapping != null && mapping.isDirty()) {
                save(mapping);
            }
        }
    }

    @Override
    @SneakyThrows(IOException.class)
    public void save(StubMapping stubMapping) {
        TestRequestContext trc = TestHeaders.extractHeaders(stubMapping, mappingsFileSource.getPath());

        String mappingFileName = fileNameMap.get(stubMapping.getId());
        if (mappingFileName == null) {
            mappingFileName = trc.getRelativeFilePath().toString();
        }
        Files.createDirectories(Paths.get(mappingsFileSource.getPath())
                .resolve(mappingFileName).getParent());
        mappingsFileSource.writeTextFile(mappingFileName, writePrivate(stubMapping));
        fileNameMap.put(stubMapping.getId(), mappingFileName);
        stubMapping.setDirty(false);
    }

    @Override
    public void remove(StubMapping stubMapping) {
        String mappingFileName = fileNameMap.get(stubMapping.getId());
        mappingsFileSource.deleteFile(mappingFileName);
        fileNameMap.remove(stubMapping.getId());
    }

    @Override
    public void removeAll() {
        for (String filename : fileNameMap.values()) {
            mappingsFileSource.deleteFile(filename);
        }
        fileNameMap.clear();
    }

    @Override
    public void loadMappingsInto(StubMappings stubMappings) {
        if (!mappingsFileSource.exists()) {
            return;
        }
        Iterable<TextFile> mappingFiles = mappingsFileSource.listFilesRecursively()
                .stream()
                .filter(AbstractFileSource.byFileExtension("json")::apply).collect(Collectors.toList());
        for (TextFile mappingFile : mappingFiles) {
            try {
                StubMapping mapping = StubMapping.buildFrom(mappingFile.readContentsAsString());
                mapping.setDirty(false);
                stubMappings.addMapping(mapping);
                fileNameMap.put(mapping.getId(), mappingFile.getPath());
            } catch (JsonException e) {
                throw new MappingFileException(mappingFile.getPath(), e.getErrors().first().getDetail());
            }
        }
    }

}
