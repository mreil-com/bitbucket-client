package com.mreil.bitbucket.api.helper.wiremock;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.stubbing.ServeEvent;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.google.common.io.PatternFilenameFilter;
import com.mreil.bitbucket.api.BitbucketApi;
import com.mreil.bitbucket.api.helper.Env;
import com.mreil.bitbucket.api.helper.feign.RequestIdInserter;
import com.mreil.bitbucket.api.helper.jupiter.TestClassContext;
import com.mreil.bitbucket.api.helper.jupiter.TestMethodContext;
import com.mreil.bitbucket.api.helper.jupiter.TestRequestContext;
import com.mreil.bitbucket.credentials.CredentialsProvider;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
public class WireMockRule implements BeforeAllCallback,
        AfterAllCallback,
        BeforeEachCallback {
    private final Path rootDir;

    private WireMockSetupHelper mockServer;

    private static final PatternFilenameFilter ENDS_WITH_JSON =
            new PatternFilenameFilter(".*\\.json");

    public WireMockRule(String rootDir) {
        this.rootDir = Paths.get(rootDir);
    }

    private ThreadLocal<TestRequestContext> trc = new ThreadLocal<>();
    private ThreadLocal<TestMethodContext> tmc = new ThreadLocal<>();
    private TestClassContext tcc;
    private Map<TestMethodContext, Integer> currentRequestCount = new HashMap<>();

    @Override
    public void beforeAll(ExtensionContext ec) {
        mockServer = WireMockSetupHelper.atRoot(rootDir);
        tcc = TestClassContext.fromContext(rootDir, ec);
    }

    @Override
    public void afterAll(ExtensionContext ec) {
        mockServer.shutdown();

        if (!Env.isLive()) {
            checkFilesUsed();
            checkAllSubdirectoriesInTestDirHaveACorrespondingTest(ec);
        }
    }

    private void checkAllSubdirectoriesInTestDirHaveACorrespondingTest(ExtensionContext ec) {
        File mappingsDir = tcc.getAbsolutePath().toFile();
        Set<String> filesInPath = Arrays.stream(mappingsDir.listFiles(File::isDirectory))
                .map(File::getName)
                .collect(Collectors.toSet());

        List<String> testNames = Arrays.stream(ec.getRequiredTestClass().getDeclaredMethods())
                .map(m -> m.getAnnotationsByType(DisplayName.class))
                .filter(a -> a.length > 0)
                .map(a -> a[0].value())
                .map(TestMethodContext::displayNameToDirName)
                .collect(Collectors.toList());

        HashSet<String> filesWithoutTests = new HashSet<>(filesInPath);
        filesWithoutTests.removeAll(testNames);

        if (!filesWithoutTests.isEmpty()) {
            fail("No tests for files: " + filesWithoutTests);
        }
    }

    private void checkFilesUsed() {
        List<UUID> servedIds = mockServer.getServeEvents().getServeEvents().stream()
                .map(ServeEvent::getStubMapping)
                .map(StubMapping::getUuid)
                .collect(Collectors.toList());

        File mappingsDir = tmc.get().getAbsolutePath().toFile();
        File[] filesInPath = mappingsDir.listFiles(ENDS_WITH_JSON);
        List<UUID> idsNotServed = Arrays.stream(filesInPath)
                .map(this::fileToJson)
                .map(json -> UUID.fromString(json.get("id").asText()))
                .filter(id -> !servedIds.contains(id))
                .collect(Collectors.toList());

        if (!idsNotServed.isEmpty()) {
            fail("Unused stub(s) in directory "
                    + mappingsDir + ": "
                    + idsNotServed);
        } else {
            log.info("All stub files used, OK!");
        }
    }


    @SneakyThrows(IOException.class)
    private JsonNode fileToJson(File f) {
        return new ObjectMapper().readTree(f);
    }

    @Override
    public void beforeEach(ExtensionContext context) {
        TestMethodContext ctx = TestMethodContext.fromContext(rootDir, context);

        skipTestIfDirExists(ctx.getAbsolutePath());
        tmc.set(ctx);
    }

    private void skipTestIfDirExists(Path absolutePath) {
        if (!Env.isLive()) {
            return;
        }

        // check if directory exists
        Assumptions.assumeFalse(absolutePath.toFile().exists(),
                "Directory already exists: " + absolutePath + ". Delete before re-running test.");
    }

    /**
     * Must be called before each API call in a test.
     * // TODO make sure ids are unique
     */
    public void nextRequest(String id) {
        TestMethodContext currentMethod = this.tmc.get();
        Integer currentRequestSeq = currentRequestCount.getOrDefault(currentMethod, 0);

        trc.set(TestRequestContext.fromMethodContext(currentMethod,
                id,
                currentRequestSeq));

        currentRequestCount.put(currentMethod, currentRequestSeq + 1);
    }

    /**
     * Get the top level URL for the API.
     * This will always be a localhost URL.
     *
     * @return the top level URL for the API.
     */
    public BitbucketApi getApiRoot() {
        return BitbucketApi.builder()
                .baseUrl("http://localhost:" + mockServer.port() + "/")
                .credentialsProvider(getUserPassCredentials())
                .interceptor(new RequestIdInserter(trc))
                .build();
    }

    /**
     * Get the top level URL for the API.
     * Do not inject a default credentials provider.
     *
     * @return the top level URL for the API.
     */
    public BitbucketApi getApiRootAnonymous() {
        return BitbucketApi.builder()
                .baseUrl("http://localhost:" + mockServer.port() + "/")
                .interceptor(new RequestIdInserter(trc))
                .build();
    }

    private CredentialsProvider getUserPassCredentials() {
        if (Env.isLive()) {
            return CredentialsHelper.correctUserPassForApi();
        } else {
            return CredentialsHelper.correctUserPassForLocalTests();
        }
    }
}
