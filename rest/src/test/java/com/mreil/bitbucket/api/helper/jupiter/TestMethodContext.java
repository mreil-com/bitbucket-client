package com.mreil.bitbucket.api.helper.jupiter;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.nio.file.Path;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TestMethodContext {
    private TestClassContext tcc;
    private String testMethodName;

    static TestMethodContext of(Path rootDir,
                                String testClass,
                                String testMethodName) {
        return new TestMethodContext(TestClassContext.of(rootDir, testClass),
                testMethodName);
    }

    /**
     * Create new context for test method.
     *
     * @param rootDir root dir of the stub mapping files (usually src/test/resources)
     * @param ec JUnit 5 extension context
     * @return test method context
     */
    public static TestMethodContext fromContext(
            Path rootDir,
            ExtensionContext ec) {
        return new TestMethodContext(
                TestClassContext.fromContext(rootDir, ec),
                displayNameToDirName(ec.getDisplayName()));
    }

    public static String displayNameToDirName(String displayName) {
        return displayName.replace(" ", "_")
                .replace("/", "_");
    }

    Path getRelativePath() {
        return tcc.getRelativePath().resolve(testMethodName);
    }

    public Path getAbsolutePath() {
        return tcc.getAbsolutePath().resolve(testMethodName);
    }

}
