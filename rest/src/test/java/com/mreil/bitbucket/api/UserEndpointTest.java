package com.mreil.bitbucket.api;

import com.mreil.bitbucket.api.helper.wiremock.WireMockRule;
import com.mreil.bitbucket.credentials.Email;
import com.mreil.bitbucket.credentials.Paginated;
import com.mreil.bitbucket.credentials.RepositoryPermission;
import com.mreil.bitbucket.credentials.TeamPermission;
import com.mreil.bitbucket.credentials.User;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import javax.servlet.http.HttpServletResponse;
import java.util.function.Supplier;

import static com.mreil.bitbucket.api.testdata.TestData.EXPECTED_BASE_USER;
import static com.mreil.bitbucket.api.testdata.TestData.EXPECTED_USER;
import static com.mreil.bitbucket.api.testdata.TestData.REPOSITORY_FULLNAME;
import static com.mreil.bitbucket.api.testdata.TestData.REPOSITORY_NAME;
import static com.mreil.bitbucket.api.testdata.TestData.REPOSITORY_UUID;
import static com.mreil.bitbucket.api.testdata.TestData.TEAMACCOUNT_DISPLAYNAME;
import static com.mreil.bitbucket.api.testdata.TestData.TEAMACCOUNT_USERNAME;
import static com.mreil.bitbucket.api.testdata.TestData.TEAMACCOUNT_UUID;
import static com.mreil.bitbucket.api.testdata.TestData.USERACCOUNT_EMAIL;
import static com.mreil.bitbucket.api.testdata.TestData.USERACCOUNT_USERNAME;
import static com.mreil.bitbucket.credentials.Permission.ADMIN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
class UserEndpointTest {
    @SuppressWarnings("WeakerAccess")
    @RegisterExtension
    static final WireMockRule mock = new WireMockRule("src/test/resources");

    private static UserEndpoint loggedIn;
    private static UserEndpoint anonymous;

    @BeforeAll
    static void getApiRoots() {
        loggedIn = mock.getApiRoot().getUserEndpoint();
        anonymous = mock.getApiRootAnonymous().getUserEndpoint();
    }

    @Test
    @DisplayName("Get current User")
    void user() {
        User user = makeCall(loggedIn::user,
                "call to user");

        assertThat(user).isNotNull();
        assertThat(user).isEqualToIgnoringNullFields(EXPECTED_USER);

        assertThat(user.getLinks().get("self").getHref())
                .isEqualToIgnoringCase("https://api.bitbucket.org/2.0/users/" + USERACCOUNT_USERNAME);
    }

    @Test
    @DisplayName("Get current User anonymous fails")
    void user_anonymous_fails() {
        FeignException ex = assertThrows(FeignException.class,
                () -> makeCall(anonymous::user, "call to user"));

        assertThat(ex.status()).isEqualTo(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    @DisplayName("Get emails")
    void get_emails() {
        Paginated<Email> emails = makeCall(loggedIn::emails, "get emails");

        assertThat(emails.getSize()).isEqualTo(1);
        assertThat(emails.getPage()).isEqualTo(1);
        assertThat(emails.getPagelen()).isEqualTo(10);
        assertThat(emails.getValues().size()).isEqualTo(1);

        Email email = emails.getValues().get(0);
        assertThat(email).isEqualToIgnoringNullFields(Email.builder()
                .emailAddress(USERACCOUNT_EMAIL)
                .type("email")
                .isPrimary(true)
                .isConfirmed(true)
                .build());

        assertThat(email.getLinks().get("self").getHref())
                .isEqualToIgnoringCase("https://api.bitbucket.org/2.0/user/emails/" + USERACCOUNT_EMAIL);
    }

    @Test
    @DisplayName("Get email")
    void get_email() {
        Email email = makeCall(() -> loggedIn.emails(USERACCOUNT_EMAIL), "get email");
        assertThat(email).isEqualToIgnoringNullFields(Email.builder()
                .emailAddress(USERACCOUNT_EMAIL)
                .type("email")
                .isPrimary(true)
                .isConfirmed(true)
                .build());
    }

    @Test
    @DisplayName("Get repo permissions")
    void get_repo_permissions() {
        Paginated<RepositoryPermission> permissions =
                makeCall(loggedIn::repositoryPermissions, "get repo permissions");
        RepositoryPermission permission = permissions.getValues().get(0);

        assertThat(permission).isEqualToIgnoringNullFields(RepositoryPermission.builder()
                .type("repository_permission")
                .permission(ADMIN)
                .build());

        assertThat(permission.getUser()).isEqualToIgnoringNullFields(EXPECTED_BASE_USER);
        assertThat(permission.getUser().getLinks()).containsOnlyKeys("self", "html", "avatar");

        RepositoryPermission.Repository expectedRepo = RepositoryPermission.Repository.builder()
                .type("repository")
                .name(REPOSITORY_NAME)
                .fullName(REPOSITORY_FULLNAME)
                .uuid(REPOSITORY_UUID)
                .build();
        assertThat(permission.getRepository()).isEqualToIgnoringNullFields(expectedRepo);
        assertThat(permission.getRepository().getLinks()).containsOnlyKeys("self", "html", "avatar");
    }

    @Test
    @DisplayName("Get team permissions")
    void get_team_permissions() {

        Paginated<TeamPermission> permissions =
                makeCall(loggedIn::teamPermissions, "get team permissions");
        TeamPermission permission = permissions.getValues().get(0);

        assertThat(permission).isEqualToIgnoringNullFields(TeamPermission.builder()
                .type("team_permission")
                .permission(ADMIN)
                .build());

        assertThat(permission.getUser()).isEqualToIgnoringNullFields(EXPECTED_BASE_USER);
        assertThat(permission.getUser().getLinks()).containsOnlyKeys("self", "html", "avatar");

        TeamPermission.Team expectedTeam = TeamPermission.Team.builder()
                .type("team")
                .username(TEAMACCOUNT_USERNAME)
                .displayName(TEAMACCOUNT_DISPLAYNAME)
                .uuid(TEAMACCOUNT_UUID)
                .build();
        assertThat(permission.getTeam()).isEqualToIgnoringNullFields(expectedTeam);
        assertThat(permission.getTeam().getLinks()).containsOnlyKeys("self", "html", "avatar");
    }

    private <X> X makeCall(Supplier<X> call, String id) {
        mock.nextRequest(id);
        return call.get();
    }
}
