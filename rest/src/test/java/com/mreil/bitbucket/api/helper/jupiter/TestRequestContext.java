package com.mreil.bitbucket.api.helper.jupiter;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.nio.file.Path;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TestRequestContext {
    private TestMethodContext tmc;

    private String requestId;

    private int count;

    /**
     * Create new context for an HTTP request made in a test.
     *
     * @param rootDir      root dir of the stub mapping files (usually src/test/resources)
     * @param testClass    name of the test class, usually extracted from the stub mapping headers
     * @param displayName  name of the test method, usually extracted from the stub mapping headers
     * @param requestId    id of the HTTP request, usually extracted from the stub mapping headers
     * @param requestCount sequence id of the HTTP request, usually extracted from the stub mapping headers
     * @return test request context
     */
    public static TestRequestContext of(Path rootDir,
                                        String testClass,
                                        String displayName,
                                        String requestId,
                                        int requestCount) {
        return new TestRequestContext(TestMethodContext.of(rootDir, testClass, displayName),
                requestId,
                requestCount);
    }

    /**
     * Create new context for an HTTP request made in a test.
     *
     * @param tmc          method context
     * @param requestId    id of the HTTP request, set during the test
     * @param requestCount sequence id of the HTTP request, auto-incremented sequence per test
     * @return test request context
     */
    public static TestRequestContext fromMethodContext(TestMethodContext tmc,
                                                       String requestId,
                                                       int requestCount) {
        return new TestRequestContext(tmc,
                requestId,
                requestCount);
    }

    public Path getRelativeFilePath() {
        String filename = tmc.getTestMethodName() + "-" + count + ".json";
        return tmc.getRelativePath().resolve(filename);
    }
}
