package com.mreil.bitbucket.api;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EventTest {
    @Test
    void eventToString() {
        assertThat(Event.ISSUE_CREATED.getKey()).isEqualTo("issue:created");
    }

    @Test
    void stringToEvent() {
        assertThat(Event.fromString("issue:created")).isEqualTo(Event.ISSUE_CREATED);
    }

    @Test
    void invalidEvent() {
        assertThrows(RuntimeException.class,
                () -> Event.fromString("doesnt:exist"));
    }
}
