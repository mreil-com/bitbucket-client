package com.mreil.bitbucket.credentials;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mreil.bitbucket.api.Links;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class RepositoryPermission extends Paginated<RepositoryPermission> {
    private String type;
    private Permission permission;


    private BaseUser user;
    private Repository repository;

    @Data
    @Builder
    public static class Repository {
        private String name;

        @JsonProperty("full_name")
        private String fullName;
        private String type;
        private String uuid;
        private Links links;
    }
}
