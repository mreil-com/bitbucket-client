package com.mreil.bitbucket.credentials;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mreil.bitbucket.api.Event;
import com.mreil.bitbucket.api.Links;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Set;

@Data
public class Hook {
    @JsonProperty("read_only")
    private boolean readOnly;

    private String description;
    private Links links;
    private String url;

    @JsonProperty("created_at")
    private ZonedDateTime createdAt;

    @JsonProperty("skip_cert_verification")
    private boolean skipCertVerification;

    private String source;
    private boolean active;
    private BaseUser subject;
    private String type;
    private Set<Event> events;
    private String uuid;
}
