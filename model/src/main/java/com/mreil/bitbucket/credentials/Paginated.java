package com.mreil.bitbucket.credentials;

import lombok.Data;

import java.util.List;
import java.util.Optional;

@Data
public class Paginated<T> {
    private int pagelen;
    private List<T> values;
    private int page;
    private int size;
    private Optional<String> next;
    private Optional<String> previous;
}
