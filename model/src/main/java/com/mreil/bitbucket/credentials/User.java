package com.mreil.bitbucket.credentials;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mreil.bitbucket.api.Links;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Optional;

@Data
@Builder
public class User {
    String username;
    Optional<String> website;

    @JsonProperty("display_name")
    String displayName;

    @JsonProperty("account_id")
    String accountId;

    @JsonProperty("created_on")
    ZonedDateTime createdOn;

    @JsonProperty("is_staff")
    boolean isStaff;

    Optional<String> location;
    String type;
    String uuid;

    Links links;
}
