package com.mreil.bitbucket.credentials;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mreil.bitbucket.api.Links;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class TeamPermission extends Paginated<TeamPermission> {
    private String type;
    private Permission permission;


    private BaseUser user;
    private Team team;

    @Data
    @Builder
    public static class Team {
        private String username;

        @JsonProperty("display_name")
        private String displayName;
        private String type;
        private String uuid;
        private Links links;
    }
}
