package com.mreil.bitbucket.credentials;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class UserPass implements CredentialsProvider {
    String username;
    String password;
}
