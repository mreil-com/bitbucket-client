package com.mreil.bitbucket.credentials;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mreil.bitbucket.api.Links;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class Email extends Paginated<Email> {
    @JsonProperty("is_primary")
    private boolean isPrimary;

    @JsonProperty("is_confirmed")
    private boolean isConfirmed;

    private String type;

    @JsonProperty("email")
    private String emailAddress;

    private Links links;
}
