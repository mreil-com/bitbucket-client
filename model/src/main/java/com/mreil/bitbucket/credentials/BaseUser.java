package com.mreil.bitbucket.credentials;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
@SuppressWarnings({"checkstyle:membername"})
public class BaseUser {
    private String username;
    private String display_name;
    private String account_id;
    private String type;
    private String uuid;
    private Map<String, Map<String, String>> links;
}
