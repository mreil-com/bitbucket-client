package com.mreil.bitbucket.credentials;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mreil.bitbucket.api.Links;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class SshKey {
    private String comment;

    @JsonProperty("last_used")
    private ZonedDateTime lastUsed;
    private String uuid;
    private Links links;
    private String label;

    @JsonProperty("created_on")
    private ZonedDateTime createdOn;
    private String key;
    private BaseUser owner;
    private String type;
}
