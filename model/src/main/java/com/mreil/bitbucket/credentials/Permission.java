package com.mreil.bitbucket.credentials;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Permission {
    ADMIN("admin"),
    WRITE("write"),
    READ("read");

    private final String key;

    Permission(String key) {
        this.key = key;
    }

    @JsonCreator
    @SuppressWarnings("javadocmethod")
    public static Permission fromString(String key) {
        return key == null
                ? null
                : Permission.valueOf(key.toUpperCase());
    }

    @JsonValue
    public String getKey() {
        return key;
    }
}
