package com.mreil.bitbucket.credentials;

import lombok.Data;

@Data
public class PipelineVariable {
    private String value;
    private boolean secured;
    private String type;
    private String uuid;
    private String key;
}
