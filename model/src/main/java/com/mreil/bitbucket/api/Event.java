package com.mreil.bitbucket.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

import static com.mreil.bitbucket.api.Event.Action.APPROVED;
import static com.mreil.bitbucket.api.Event.Action.COMMENT_CREATED;
import static com.mreil.bitbucket.api.Event.Action.COMMENT_DELETED;
import static com.mreil.bitbucket.api.Event.Action.COMMENT_UPDATED;
import static com.mreil.bitbucket.api.Event.Action.COMMIT_COMMENT_CREATED;
import static com.mreil.bitbucket.api.Event.Action.COMMIT_STATUS_CREATED;
import static com.mreil.bitbucket.api.Event.Action.COMMIT_STATUS_UPDATED;
import static com.mreil.bitbucket.api.Event.Action.CREATED;
import static com.mreil.bitbucket.api.Event.Action.DELETED;
import static com.mreil.bitbucket.api.Event.Action.FORK;
import static com.mreil.bitbucket.api.Event.Action.FULFILLED;
import static com.mreil.bitbucket.api.Event.Action.IMPORTED;
import static com.mreil.bitbucket.api.Event.Action.PUSH;
import static com.mreil.bitbucket.api.Event.Action.REJECTED;
import static com.mreil.bitbucket.api.Event.Action.TRANSFER;
import static com.mreil.bitbucket.api.Event.Action.UNAPPROVED;
import static com.mreil.bitbucket.api.Event.Action.UPDATED;
import static com.mreil.bitbucket.api.Event.Scope.ISSUE;
import static com.mreil.bitbucket.api.Event.Scope.PROJECT;
import static com.mreil.bitbucket.api.Event.Scope.PULLREQUEST;
import static com.mreil.bitbucket.api.Event.Scope.REPO;

public enum Event {
    ISSUE_COMMENT_CREATED(ISSUE, COMMENT_CREATED),
    ISSUE_CREATED(ISSUE, CREATED),
    ISSUE_UPDATED(ISSUE, UPDATED),
    PROJECT_UPDATED(PROJECT, UPDATED),
    PULLREQUEST_APPROVED(PULLREQUEST, APPROVED),
    PULLREQUEST_COMMENT_CREATED(PULLREQUEST, COMMENT_CREATED),
    PULLREQUEST_COMMENT_DELETED(PULLREQUEST, COMMENT_DELETED),
    PULLREQUEST_COMMENT_UPDATED(PULLREQUEST, COMMENT_UPDATED),
    PULLREQUEST_CREATED(PULLREQUEST, CREATED),
    PULLREQUEST_FULFILLED(PULLREQUEST, FULFILLED),
    PULLREQUEST_REJECTED(PULLREQUEST, REJECTED),
    PULLREQUEST_UNAPPROVED(PULLREQUEST, UNAPPROVED),
    PULLREQUEST_UPDATED(PULLREQUEST, UPDATED),
    REPO_COMMINT_COMMENT_CREATED(REPO, COMMIT_COMMENT_CREATED),
    REPO_COMMIT_STATUS_CREATEDX(REPO, COMMIT_STATUS_CREATED),
    REPO_COMMIT_STATUS_UPDATED(REPO, COMMIT_STATUS_UPDATED),
    REPO_CREATED(REPO, CREATED),
    REPO_DELETED(REPO, DELETED),
    REPO_FORK(REPO, FORK),
    REPO_IMPORTED(REPO, IMPORTED),
    REPO_PUSH(REPO, PUSH),
    REPO_TRANSFER(REPO, TRANSFER),
    REPO_UPDATED(REPO, UPDATED),
    ;

    private final Scope scope;
    private final Action action;

    Event(Scope scope, Action action) {
        this.scope = scope;
        this.action = action;
    }

    public enum Scope {
        ISSUE,
        PROJECT,
        PULLREQUEST,
        REPO,
    }

    public enum Action {
        APPROVED,
        COMMENT_CREATED,
        COMMENT_DELETED,
        COMMENT_UPDATED,
        COMMIT_COMMENT_CREATED,
        COMMIT_STATUS_CREATED,
        COMMIT_STATUS_UPDATED,
        CREATED,
        DELETED,
        FORK,
        FULFILLED,
        IMPORTED,
        PUSH,
        REJECTED,
        TRANSFER,
        UNAPPROVED,
        UPDATED,
    }

    @JsonCreator
    @SuppressWarnings("javadocmethod")
    public static Event fromString(String key) {
        String[] parts = key.split(":");
        return Arrays.stream(Event.values())
                .filter(p -> p.scope == Scope.valueOf(parts[0].toUpperCase()))
                .filter(p -> p.action == Action.valueOf(parts[1].toUpperCase()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Invalid event: " + key));
    }

    /**
     * Build key from components.
     *
     * @return colon-separated key used in JSON
     */
    @JsonValue
    public String getKey() {
        return scope.toString().toLowerCase()
                + ":"
                + action.toString().toLowerCase();
    }
}
