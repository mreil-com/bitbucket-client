package com.mreil.bitbucket.api;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.Set;

@Data
@Builder
public class ModifyHookCommand {
    @NonNull
    private Set<Event> events;

    @NonNull
    private Boolean active;

    @NonNull
    private String url;

    private String description;
}
