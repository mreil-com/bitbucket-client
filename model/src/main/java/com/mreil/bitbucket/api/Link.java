package com.mreil.bitbucket.api;

import lombok.Data;

import java.util.Optional;

@Data
public class Link {
    private String href;
    private Optional<String> name = Optional.empty();
}
