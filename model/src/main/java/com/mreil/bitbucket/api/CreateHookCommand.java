package com.mreil.bitbucket.api;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.Set;

@Data
@Builder
public class CreateHookCommand {
    private Set<Event> events;

    @Builder.Default
    private boolean active = true;

    @NonNull
    private String url;

    private String description;
}
