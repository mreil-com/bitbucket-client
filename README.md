# gradle-starter

[![License][license-badge]][license]
[![Download][bintray-badge]][bintray-link]
[![Sonarqube][sonarqube-badge]][sonarqube-link]
[![Coverage][codecov-badge]][codecov]
[![standard-readme compliant][readme-badge]][readme-link]

> Gradle starter project

Start a [Gradle][gradle] project quickly.


## Install

Clone the repository.


## Usage


## Build

### Run Tests

#### Live API Tests

##### Test Account Setup

- has a team
- has a repository
- pipelines enabled
- a pipelines variable configured

## Changes

See [CHANGELOG.md][changelog] for changes.


## Maintainer

[Markus Reil](https://bitbucket.org/mreil-com/)


## Contribute

Please raise issues [here](../../issues).

Feel free to address existing issues and raise a [pull request](../../pull-requests).


## License

[MIT][license]

[sonarqube-badge]: https://sonarcloud.io/api/project_badges/measure?project=com.mreil.bitbucket%3Abitbucket-client-parent&metric=alert_status
[sonarqube-link]: https://sonarcloud.io/dashboard?id=com.mreil.bitbucket%3Abitbucket-client-parent
[readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[readme-link]: https://github.com/RichardLitt/standard-readme
[bintray-badge]: https://com.mreil.bitbucket.api.bintray.com/packages/mreil/XXX/images/download.svg
[bintray-link]: https://bintray.com/mreil/XXX
[codecov-badge]: https://codecov.io/bb/mreil-com/bitbucket-client/branch/master/graph/badge.svg
[codecov]: https://codecov.io/bb/mreil-com/bitbucket-client
[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
[license]: LICENSE
[changelog]: CHANGELOG.md
[gradle]: https://gradle.org/  "gradle.org"
